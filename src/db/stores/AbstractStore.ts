import * as DataStore from 'nedb';
import { AbstractEntity } from '../../module/AbstractEntity';
import logger from '../../logging/logger';

export abstract class AbstractStore {
    protected promisify = require('util').promisify;

    protected dataPath: string;

    protected db: DataStore;

    public constructor() {
      this.dataPath = this.getDataFilePath();
      this.db = new DataStore({ filename: this.dataPath, autoload: true });
    }

    protected abstract getDataFilePath(): string;

    protected abstract getNewEntity(args: Record<string, any>): AbstractEntity;

    public async findById(id: string): Promise<any> {
      const findOne = this.promisify(this.db.findOne.bind(this.db));

      const document = await findOne({ _id: id });

      return this.getNewEntity(document);
    }

    public async findByAttributes(attributes: Record<string, any>): Promise<Array<any>> {
      const find = this.promisify(this.db.find.bind(this.db));
      const documents = await find(attributes);

      const results = documents.map((document: Document) => this.getNewEntity(document));

      return results;
    }

    public async insert(attributes: any): Promise<AbstractEntity> {
      const insert: Function = this.promisify(this.db.insert.bind(this.db));
      attributes.created = Date.now();

      const document = await insert(attributes);

      return this.getNewEntity(document);
    }

    public async update(id: string, attributes: any): Promise<AbstractEntity> {
      const update: Function = this.promisify(this.db.update.bind(this.db));

      const document = await update({ _id: id }, attributes, {});

      return this.getNewEntity(document);
    }
}
