import * as DataStore from 'nedb';
import { User, UserAssignable } from '../../module/user/User';
import { AbstractStore } from './AbstractStore';

export class UserStore extends AbstractStore {
  public constructor() {
    super();
  }

  protected getDataFilePath(): string {
    return 'data/users.db';
  }

  protected getNewEntity(args: UserAssignable): User {
    return new User(args);
  }
}
