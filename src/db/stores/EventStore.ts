import * as DataStore from 'nedb';
import { Event, EventAssignable } from '../../module/event/Event';
import { AbstractStore } from './AbstractStore';

export class EventStore extends AbstractStore {
  public constructor() {
    super();
  }

  protected getDataFilePath(): string {
    return 'data/events.db';
  }

  protected getNewEntity(args: EventAssignable): Event {
    return new Event(args);
  }
}
