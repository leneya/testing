import { initialize } from './initialize';

async function init(): Promise<void> {
  // async actions needed too be completed before setup
  initialize();
}

init();
