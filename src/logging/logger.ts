import * as winston from 'winston';

function initializeLogging(): winston.Logger {
  const logger = winston.createLogger({
    transports: [
      new winston.transports.Console(),
    ],
  });

  return logger;
}

const logger: winston.Logger = initializeLogging();

export default logger;
