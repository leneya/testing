import { AbstractEntity, EntityData } from '../AbstractEntity';

export interface UserAssignable extends EntityData {
    _id?: string;
    email: string;
    password: string;
    phone?: string;
}

export class User extends AbstractEntity {
    protected _id?: string;

    protected email: string;

    protected password: string;

    protected phone?: string;

    public constructor(data: UserAssignable) {
      super(data);

      const {
        _id, email, password, phone,
      } = data;

      this._id = _id;
      this.email = email;
      this.password = password;
      this.phone = phone;
    }

    public setEmail(email: string) {
      this.email = email;
    }

    public setPassword(password: string) {
      this.password = password;
    }

    public setPhone(phone: string) {
      this.phone = phone;
    }
}
