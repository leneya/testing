import { UserStore } from '../../db/stores/UserStore';
import { User, UserAssignable } from './User';
import { AbstractEntity } from '../AbstractEntity';

export interface SearchableUserAttributes {
    email?: string;
    phone?: string;
}

export default class UserRepository {
    store: UserStore;

    public constructor(store: UserStore) {
      this.store = store;
    }

    public async add(attributes: UserAssignable): Promise<AbstractEntity> {
      const user = this.store.insert(attributes);
      return user;
    }

    public async findAll(): Promise<User[]> {
      const users = this.store.findByAttributes({});
      return users;
    }

    public async findBy(attributes: SearchableUserAttributes): Promise<User[]> {
      const users = this.store.findByAttributes(attributes);
      return users;
    }

    public async findById(id: string): Promise<User> {
      const user = this.store.findById(id);
      return user;
    }

    public async update(id: string, attributes: UserAssignable): Promise<AbstractEntity> {
      const user = this.store.update(id, attributes);
      return user;
    }
}
