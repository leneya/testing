import { isUndefined } from 'util';
import { UserAssignable } from './User';
import UserRepository from './UserRepository';

export class UserValidator {
    protected repo: UserRepository;

    protected errors: Array<string> = [];

    public constructor(repo: UserRepository) {
      this.repo = repo;
    }

    public async isValidUser(attributes: UserAssignable): Promise<boolean> {
      this.errors = [];
      let phoneValid = true;
      const { email, password, phone } = attributes;

      if (!isUndefined(phone)) {
        phoneValid = this.isValidPhone(phone);
      }

      return phoneValid && await this.isUniqueEmail(email) && this.isValidPassword(password);
    }

    public getErrors(): Array<string> {
      return this.errors;
    }

    private async isUniqueEmail(email: string): Promise<boolean> {
      let documents = [];

      documents = await this.repo.findBy({ email });

      const result = documents.length <= 0;

      if (!result) {
        this.errors.push('This email has already been registered.');
      }

      return result;
    }

    private isValidPhone(phone: string): boolean {
      const format = /^\(?([0-9]{3})\)?[-]([0-9]{3})[-]([0-9]{4})$/;

      const result = format.test(phone);

      if (!result) {
        this.errors.push('Phone number is invalid. Please follow the pattern 555-555-5555');
      }

      return result;
    }

    private isValidPassword(password: string|undefined) {
      const result = !isUndefined(password);

      if (!result) {
        this.errors.push('A password is required to register.');
      }

      return result;
    }
}
