import { EventStore } from '../../db/stores/EventStore';
import { Event, EventAssignable } from './Event';
import { AbstractEntity } from '../AbstractEntity';

export interface SearchableEventAttributes {
    type?: string;
    userId?: string;
}

export default class EventRepository {
    store: EventStore;

    public constructor(store: EventStore) {
      this.store = store;
    }

    public async add(attributes: EventAssignable): Promise<AbstractEntity> {
      const event = this.store.insert(attributes);
      return event;
    }

    public async findAll(): Promise<Event[]> {
      const events = this.store.findByAttributes({});
      return events;
    }

    public async findAllForDate(begin: number, end: number): Promise<Event[]> {
      if (end < begin) {
        throw new Error('Start date cannot be less than end date.');
      }

      const attributes = { created: { $gte: begin, $lte: end } };

      const events = this.store.findByAttributes(attributes);

      return events;
    }

    public async findBy(attributes: SearchableEventAttributes): Promise<Event[]> {
      const events = this.store.findByAttributes(attributes);
      return events;
    }

    public async findById(id: string): Promise<Event> {
      const event = this.store.findById(id);
      return event;
    }

    public async update(id: string, attributes: EventAssignable): Promise<AbstractEntity> {
      const event = this.store.update(id, attributes);
      return event;
    }
}
