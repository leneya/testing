import { AbstractEntity, EntityData } from '../AbstractEntity';

export interface EventAssignable extends EntityData {
    _id?: string;
    type: string;
    userId: string;
}

export class Event extends AbstractEntity {
    protected _id?: string;

    protected type: string;

    protected userId: string;

    public constructor(data: EventAssignable) {
      super(data);

      const { _id, type, userId } = data;

      this._id = _id;
      this.type = type;
      this.userId = userId;
    }

    public setType(type: string) {
      this.type = type;
    }

    public setUserId(userId: string) {
      this.userId = userId;
    }
}
