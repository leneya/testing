import { isUndefined } from 'util';

export interface EntityData {
    created?: number;
}

export class AbstractEntity {
    protected created?: number;

    public constructor(data: EntityData) {
      this.created = data.created;
    }
}
