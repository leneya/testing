import { Request, Response } from 'express';
import UserRepository from '../../../module/user/UserRepository';


export class UserController {
    private repo: UserRepository;

    public constructor(repo: UserRepository) {
      this.repo = repo;
    }

    public async getAll(req: Request, res: Response) {
      const users = await this.repo.findAll();
      return res.status(200).send(users);
    }

    public async get(req: Request, res: Response) {
      const user = await this.repo.findById(req.body.id);
      return res.status(200).send(user);
    }

    public async post(req: Request, res: Response) {
      const user = await this.repo.add(req.body);
      return res.status(201).send(user);
    }

    public async patch(req: Request, res: Response) {
      const user = await this.repo.findById(req.body.id);

      return res.status(200).send(user);
    }
}
