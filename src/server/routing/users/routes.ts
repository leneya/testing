import * as express from 'express';
import UserRepository from '../../../module/user/UserRepository';
import { UserStore } from '../../../db/stores/UserStore';
import { UserController } from './UserController';
import { UserValidator } from '../../../module/user/UserValidator';

export default function registerUserRoutes(app: express.Express): void {
  const repo = new UserRepository(new UserStore());
  const controller = new UserController(repo);
  const validator = new UserValidator(repo);

  app.get('/users', (req: express.Request, res: express.Response) => controller.getAll(req, res));

  app.get('/users/:id', (req: express.Request, res: express.Response) => controller.get(req, res));

  app.post('/users', async (req: express.Request, res: express.Response, next: express.NextFunction) => {
    if (await validator.isValidUser(req.body)) {
      next();
    } else {
      res.status(400).send(validator.getErrors());
    }
  }, (req: express.Request, res: express.Response, next: express.NextFunction) => controller.post(req, res));

  app.patch('/users/:id', (req: express.Request, res: express.Response) => controller.patch(req, res));
}
