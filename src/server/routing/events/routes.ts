import * as express from 'express';
import EventRepository from '../../../module/event/EventRepository';
import { EventStore } from '../../../db/stores/EventStore';
import { EventController } from './EventController';

export default function registerEventRoutes(app: express.Express): void {
  // generally this should be taken care of inside of some injection container.
  const repo = new EventRepository(new EventStore());
  const controller = new EventController(repo);

  app.get('/events', (req: express.Request, res: express.Response) => controller.getAll(req, res));

  app.get('/events/:id', (req: express.Request, res: express.Response) => controller.get(req, res));

  app.get('/users/:userId/events', (req: express.Request, res: express.Response) => controller.getForUser(req, res));

  app.post('/events', (req: express.Request, res: express.Response) => controller.post(req, res));

  app.patch('/events/:id', (req: express.Request, res: express.Response) => controller.patch(req, res));
}
