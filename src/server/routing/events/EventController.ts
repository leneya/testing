import { Request, Response } from 'express';
import { isUndefined } from 'util';
import EventRepository from '../../../module/event/EventRepository';
import { Event } from '../../../module/event/Event';


export class EventController {
    private repo: EventRepository;

    public constructor(repo: EventRepository) {
      this.repo = repo;
    }

    public async getAll(req: Request, res: Response) {
      let events = [];

      if (!isUndefined(req.query.today)) {
        events = await this.getForToday();
      } else {
        events = await this.repo.findAll();
      }
      return res.status(200).send(events);
    }

    public async getForUser(req: Request, res: Response) {
      const events = await this.repo.findBy({ userId: req.params.userId });
      return res.status(200).send(events);
    }

    public async get(req: Request, res: Response) {
      const event = await this.repo.findById(req.body.id);
      return res.status(200).send(event);
    }

    public async post(req: Request, res: Response) {
      const event = await this.repo.add(req.body);
      return res.status(201).send(event);
    }

    public async patch(req: Request, res: Response) {
      const event = await this.repo.findById(req.body.id);

      return res.status(200).send(event);
    }

    protected async getForToday(): Promise<Event[]> {
      const start = new Date();
      start.setHours(0, 0, 0, 0);

      const startTime = start.getTime();

      const end = new Date();
      end.setHours(23, 59, 59, 999);

      const endTime = end.getTime();

      return this.repo.findAllForDate(startTime, endTime);
    }
}
