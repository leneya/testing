import * as express from 'express';
import registerUserRoutes from './users/routes';
import registerEventRoutes from './events/routes';

function initializeRouter(app: express.Express): void {
  if (process.env.APP_ENV === 'development') {
    app.get(`/${process.env.APP_KEY}`, (req: express.Request, res: express.Response) => res.sendStatus(422));
  }

  registerUserRoutes(app);
  registerEventRoutes(app);
}

export default initializeRouter;
