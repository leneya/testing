import * as express from 'express';
import logger from '../../logging/logger';

import bodyParser = require('body-parser');

export const initializeMiddleware = (app: express.Express): void => {
  app.use(bodyParser.urlencoded({
    extended: true,
  }));

  app.use(bodyParser.json());

  app.use((request: express.Request, response: express.Response, next: express.NextFunction) => {
    logger.log({
      level: 'info',
      message: `request made to ${request.url}`,
    });

    next();
  });
};

export default initializeMiddleware;
