import 'reflect-metadata';
import * as express from 'express';
import initializeRouter from './server/routing';
import initializeMiddleware from './server/middleware';
import logger from './logging/logger';

/**
 * Initialize the app and router.
 */
export const initialize = (): express.Express => {
  const env = process.env.APP_ENV ? process.env.APP_ENV : 'production';

  const app = express();
  app.set('env', env);

  // architecture initialization
  initializeMiddleware(app);
  initializeRouter(app);

  app.listen(3000, () => logger.log({
    level: 'info',
    message: 'Server Started From App.',
  }));

  return app;
};

export default initialize;
