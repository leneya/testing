#NS8 REST API

### Assumptions
1. I don't need to encrypt passwords. If I did need to, I'd probably use something like passport for authentication, or would use the bcrypt algorithm with a salt to encrypt it. 

2. I don't need to authenticate requests. If I did need to, I would create an authentication middleware and group authenticated routes beneath it. 

### What I Would Have Done Differently Now That I'm Done

1. I went down a backwards route of writing my own ORM. Would have ripped that out in favor
of TypeORM (because it works better with TypeScript imo) which would have saved time and would have
been a better solution. 

2. Automated testing using JEST at both the route (integration) and unit levels. 

3. I would have validated that a new event tied to an actual existing user. 

4. Would have fixed the 51 linting issues. 

5. I would have cleaned up (any) return types. 

6. I would have created a docker container to ensure that environments were stable. 